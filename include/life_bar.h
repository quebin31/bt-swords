#ifndef SWORDS_LIFE_BAR_H
#define SWORDS_LIFE_BAR_H

#include "swords_include.h"
#include "bitmap.h"

class Segment : public Bitmap {
public:
  Segment();

  void DrawBitmap(const int& flags);
  void ResetBitmap();
};


const int8_t NO_SEGMENTS = 20;

class LifeBar {
private:
  Bitmap  *segments;
  int8_t   position;
  int64_t  frame_stamp;

public:
  LifeBar() = default;
  LifeBar(double start);
  ~LifeBar();

  const bool IsEmpty();
  void ReduceOneSegment(const int64_t& stamp);

  void DrawBar();
};

#endif
