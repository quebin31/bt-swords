#ifndef SWORDS_GAME_H
#define SWORDS_GAME_H

#include "swords_include.h"
#include "life_bar.h"

const uint8_t THERE_IS_ALREADY_AN_INSTATION = (1 << 0);
const uint8_t DONE_MAIN = (1 << 1);
const uint8_t GAMEOVER_OR_PAUSE = (1 << 2);

class FrameworkGame {
private:
  // Allegro things
  AllegroDisplay    *display;
  AllegroEventQueue *event_queue;
  AllegroTimer      *timer;

  // Bluetooth things
  bluez::BTClient   *me;
  char               msg[2];

  // Objects and states
  LifeBar           *bar;
  static uint8_t     flags;

  void SomethingGoesWrong(const char* failed, int line);

public:
  FrameworkGame();
  FrameworkGame(bluez::BTClient *me);
  ~FrameworkGame();

  int GetWidthScreen();
  int GetHeightScreen();

  void SetDisplayColor(unsigned char r, unsigned char g, unsigned char b);

  bool GetMainActivityState();
  bool GetGameActivityState();
  void SetFlags(const uint8_t new_flags);

  void ShowMenu();
  void StartTimer();
  void WaitForEvent(AllegroEvent &event);
  void ManageEvent(AllegroEvent &event);
  void EventTimer();

  int64_t GetTimerCount();

  void ResetGame();
};

#endif
