#ifndef SWORDS_BITMAP_H
#define SWORDS_BITMAP_H

#include "swords_include.h"

class Bitmap {
protected:
  AllegroBitmap *bitmap;
  int           width, height;
  double        pos_x, pos_y;
  double        source_x, source_y;
  bool          destroyed;

public:
  Bitmap(const char* file);
  virtual ~Bitmap();

  const double GetX();
  const double GetY();
  void SetX(const double& x);
  void SetY(const double& y);
  void MoveX(const double& dx);
  void MoveY(const double& dy);

  const int GetWidth();
  const int GetHeight();

  void SetSourceX(const int& x);
  void SetSourceY(const int& y);

  void SetDestroyed(const bool& state);
  const bool IsDestroyed();

  void ChangeBitmap(const char* new_file);

  virtual void DrawBitmap(const int& flags) = 0;
  virtual void ResetBitmap() = 0;
};

#endif
