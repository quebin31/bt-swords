# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/kevin/Documents/Programming/C++/Swords/src/bitmap.cc" "/home/kevin/Documents/Programming/C++/Swords/build/CMakeFiles/Swords.dir/src/bitmap.cc.o"
  "/home/kevin/Documents/Programming/C++/Swords/src/game.cc" "/home/kevin/Documents/Programming/C++/Swords/build/CMakeFiles/Swords.dir/src/game.cc.o"
  "/home/kevin/Documents/Programming/C++/Swords/src/life_bar.cc" "/home/kevin/Documents/Programming/C++/Swords/build/CMakeFiles/Swords.dir/src/life_bar.cc.o"
  "/home/kevin/Documents/Programming/C++/Swords/src/main.cc" "/home/kevin/Documents/Programming/C++/Swords/build/CMakeFiles/Swords.dir/src/main.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
