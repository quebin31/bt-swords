#include "game.h"

void FrameworkGame::SomethingGoesWrong(const char* failed, int line) {
  al_show_native_message_box(display, "Ups!", "Well...", "Something goes wrong", NULL, ALLEGRO_MESSAGEBOX_ERROR);
  printf("FrameworkGame (line %i): Failed to %s\n", line, failed);
  exit(EXIT_FAILURE);
}

uint8_t FrameworkGame::flags = 0;

FrameworkGame::FrameworkGame() {
  if (FrameworkGame::flags & THERE_IS_ALREADY_AN_INSTATION)
    throw AnotherFrameworkGameInstation();

  printf("FrameworkGame (line 15): Starting Allegro.\n");
  if (!al_init()) {
    SomethingGoesWrong("start Allegro", 17);
  }

  printf("FrameworkGame (line 20): Starting Allegro Image.\n");
  if (!al_init_image_addon()) {
    SomethingGoesWrong("start Allegro Image", 22);
  }

  printf("FrameworkGame (line 25): Starting Allegro Font.\n");
  if (!al_init_font_addon()) {
    SomethingGoesWrong("start Allegro Font", 27);
  }

  printf("FrameworkGame (line 30): Starting Allegro TTF.\n");
  if (!al_init_ttf_addon()) {
    SomethingGoesWrong("start Allegro TTF", 32);
  }

  printf("FrameworkGame (line 35): Creating display.\n");
  display = al_create_display(screen.width, screen.height);
  if (!display) {
    SomethingGoesWrong("create display", 38);
  }
  printf("FrameworkGame (line 40): Created display (640x240).\n");

  printf("FrameworkGame (line 42): Creating event queue.\n");
  event_queue = al_create_event_queue();
  if (!event_queue) {
    SomethingGoesWrong("create event queue", 45);
    al_destroy_display(display);
  }
  printf("FrameworkGame (line 48): Created event queue.\n");

  printf("FrameworkGame (line 50): Creating timer.\n");
  timer = al_create_timer(1.0/screen.fps);
  if (!timer) {
    SomethingGoesWrong("create timer", 53);
    al_destroy_display(display);
    al_destroy_event_queue(event_queue);
  }
  printf("FrameworkGame (line 57): Created timer.\n");

  printf("FrameworkGame (line 59): Registering events.\n");
  al_register_event_source(event_queue, al_get_display_event_source(display));
  al_register_event_source(event_queue, al_get_timer_event_source(timer));

  al_set_window_title(display, "Swords!");
  al_set_window_position(display, 350, 180);
  SetDisplayColor(26,26,26);

  bar = new LifeBar[2] {
    LifeBar(10),
    LifeBar(screen.width/2.0 + 10)
  };

  printf("FrameworkGame (line 66): FrameworkGame is ready!\n");
}

FrameworkGame::FrameworkGame(bluez::BTClient *me): FrameworkGame() {
  this->me = me;
  this->msg[0] = '0';
  this->msg[1] = '0';
}

FrameworkGame::~FrameworkGame() {
  al_destroy_display(display);
  al_destroy_timer(timer);
  al_destroy_event_queue(event_queue);
}

int FrameworkGame::GetWidthScreen() { return screen.width; }
int FrameworkGame::GetHeightScreen() { return screen.height; }

void FrameworkGame::SetDisplayColor(unsigned char r, unsigned char g, unsigned char b) {
  al_set_target_bitmap(al_get_backbuffer(display));
  al_clear_to_color(al_map_rgb(r,g,b));
}

bool FrameworkGame::GetMainActivityState() {
  return (FrameworkGame::flags & DONE_MAIN);
}

bool FrameworkGame::GetGameActivityState() {
  return (FrameworkGame::flags & GAMEOVER_OR_PAUSE);
}

void FrameworkGame::SetFlags(const uint8_t new_flags) {
  bool temp = false;
  if (FrameworkGame::flags & THERE_IS_ALREADY_AN_INSTATION)
    temp = true;
  FrameworkGame::flags = new_flags;
  if (temp) {
    flags |= THERE_IS_ALREADY_AN_INSTATION;
  }
}

void FrameworkGame::WaitForEvent(AllegroEvent &event) {
  al_wait_for_event(event_queue, &event);
}

void FrameworkGame::ManageEvent(AllegroEvent &event) {
  if (event.type == ALLEGRO_EVENT_TIMER)
    EventTimer();
  else if (event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
    exit(EXIT_SUCCESS);
}

void FrameworkGame::StartTimer() {
  al_start_timer(timer);
  flags &= ~GAMEOVER_OR_PAUSE;
}

void FrameworkGame::EventTimer() {
  int64_t timer_count = GetTimerCount();
  me->ReadFrom(&msg[0], 1, 0);
  me->ReadFrom(&msg[1], 1, 1);
  if (msg[0] == '1') bar[0].ReduceOneSegment(timer_count);
  if (msg[1] == '1') bar[1].ReduceOneSegment(timer_count);
  if (bar[0].IsEmpty() && bar[1].IsEmpty()) {
    printf("Draw!\n");
    exit(EXIT_SUCCESS);
  } else if (bar[0].IsEmpty()) {
    printf("Player 2 won!\n");
    exit(EXIT_SUCCESS);
  } else if (bar[1].IsEmpty()) {
    printf("Player 1 won!\n");
    exit(EXIT_SUCCESS);
  }
  // Draw results on screen
  SetDisplayColor(26,26,26);
  bar[0].DrawBar();
  bar[1].DrawBar();
  al_flip_display();
}

int64_t FrameworkGame::GetTimerCount() {
  return al_get_timer_count(timer);
}

void FrameworkGame::ResetGame() {
  flags &= ~GAMEOVER_OR_PAUSE;
}
