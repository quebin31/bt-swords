#include "life_bar.h"

Segment::Segment(): Bitmap("segment.png") {
  width  = 15;
  height = 25;
}

void Segment::DrawBitmap(const int& flags) {
  al_draw_bitmap_region(bitmap, source_x, source_y, width, height, pos_x, pos_y, flags);
}

void Segment::ResetBitmap() {
  source_x = 0;
}

LifeBar::LifeBar(double start) {
  segments = new Segment[NO_SEGMENTS];
  position = NO_SEGMENTS - 1;
  frame_stamp = -20;
  for (int8_t i = 0; i < NO_SEGMENTS; ++i) {
    segments[i].SetY(screen.height/2.0 - segments[i].GetHeight()/2.0);
    segments[i].SetX(start);
    start += segments[i].GetWidth();
  }
}

LifeBar::~LifeBar() {
  delete[] segments;
}

const bool LifeBar::IsEmpty() {
  return (position < 0);
}

void LifeBar::ReduceOneSegment(const int64_t& stamp) {
  if (stamp < frame_stamp + 20) return;
  segments[position].SetSourceX(segments[position].GetWidth());
  position -= 1;
  frame_stamp = stamp;
}

void LifeBar::DrawBar() {
  for (int8_t i = 0; i < NO_SEGMENTS; ++i) {
    segments[i].DrawBitmap(0);
  }
}
