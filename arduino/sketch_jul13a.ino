// Para usar solo 8 bits
#include <stdint.h>

// Donde estara conectada la espada
#define sword_pin 13

// Por ahora los 3 marcan HIGH, cuando uno de ellos es tocado
#define input 7

void setup() {
  Serial.begin(9600);
  pinMode(sword_pin, OUTPUT);
  pinMode(input, INPUT);
}

void loop() {
  // sword_pin tiene que estar en HIGH siempre
  digitalWrite(sword_pin, HIGH);
  // Si es que alguna de las entradas esta en HIGH
  // mandemos '1' como señal, si no '0'
  if (digitalRead(input) == HIGH) {
    Serial.write('1');
  } else {
    Serial.write('0');
  }
  delay(350);
}
